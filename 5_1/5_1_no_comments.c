#include <stdio.h>

int main() {
	int n = 0;
	printf("Введите n (количество строк): ");
    scanf("%d", &n);	
	if(n >= 4 && n <= 10) {
		int m = 0;
		printf("Введите m (количество столбцов): ");
		scanf("%d", &m);		
		if(m >= 4 && m <= 10) {
			int X[n][m];
			for(int i = 0; i < n; i++) {
				for(int j = 0; j < m; j++) {
					printf("\nВведите элемент матрицы\n%d-я строка\n%d-й столбец\nX[%d][%d]: ", i + 1, j + 1, i + 1, j + 1);
					scanf("%d", &X[i][j]);
				} 
			}
			printf("\nНомера столбцов, где нет ни одного нулевого элемента\n");
			int countColumns = 0;
			for(int j = 0; j < m; j++) {
				int zeroCount = 0;
				for(int i = 0; i < n; i++) {
					if(X[i][j] == 0) {
						zeroCount += 1;
					}
				}
				if(zeroCount == 0) {
					countColumns += 1;
					printf("%d ", j + 1);
				}
			}
			if(countColumns == 0) {
				printf("Таких столбцов нет");
			}
		} else {
			printf("Количество столбцов должно быть больше или равно 4 и меньше или равно 10");
		}
	} else {
		printf("Количество строк должно быть больше или равно 4 и меньше или равно 10");
	}
	return 0;
}