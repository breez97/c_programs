#include <stdio.h>

int main() {
	int amount = 0;
	float sum = 0;
	int n = 0;
	scanf("%d", &n);

	if (n > 0) {
		float X[n];
		float *ptr;

		for(ptr = X; ptr < X + n; ptr++) {
			scanf("%f", ptr);
		}

		for(ptr = X; ptr < X + n; ptr++) {
			if (*ptr > (ptr - X)) {
				amount += 1;
			}

			if ((int)*ptr != *ptr) {
				sum += *ptr;
			}
		}

		if (amount == 0) {
			printf("Элементов, значение которых больше индекса, нет\n");
		} else {
			printf("Количество элементов, значение которых больше индекса: %d\n", amount);
		}

		if (sum == 0) {
			printf("В массиве нет элементов с дробной частью\n");
		} else {
			printf("Сумма элементов, у которых есть дробная часть: %f\n", sum);
		}
	} 
	else {
		printf("Ошибка ввода размера массива, число должно быть больше 0\n");
	}

	return 0;
}