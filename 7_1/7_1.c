#include <stdio.h>

void MakeTask(float *X, int n, int *amount, float *sum);

void FillArray(float *X, int n);
void GetAmountAndSum(float *X, int n, int *amount, float *sum);
void CheckAmount(int amount);
void CheckSum(float sum);

int main() {
    int amount = 0;
    float sum = 0;
    int n = 0;
    scanf("%d", &n);
	float X[n];

    MakeTask(X, n, &amount, &sum);

    return 0;
}

void MakeTask(float *X, int n, int *amount, float *sum) {
    if(n > 0) {

        FillArray(X, n);

        GetAmountAndSum(X, n, amount, sum);

        CheckAmount(*amount);

        CheckSum(*sum);
    } else {
        printf("Ошибка ввода размера массива, число должно быть больше 0\n");
    }
}

void FillArray(float *X, int n) {
    for(int i = 0; i < n; i++) {
        scanf("%f", &X[i]);
    }
}

void GetAmountAndSum(float *X, int n, int *amount, float *sum) {
    for(int i = 0; i < n; i++) {
        if(X[i] > i) {
            (*amount)++;
        }

        if((int)X[i] != X[i]) {
            *sum += X[i];
        }
    }
}

void CheckAmount(int amount) {
    if(amount == 0) {
        printf("Элементов, значение которых больше индекса, нет\n");
    } else {
        printf("Количество элементов, значение которых больше индекса: %d\n", amount);
    }
}

void CheckSum(float sum) {
    if(sum == 0) {
        printf("В массиве нет элементов с дробной частью\n");
    } else {
        printf("Сумма элементов, у которых есть дробная часть: %f\n", sum);
    }
}