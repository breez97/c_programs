#include <stdio.h>

int main() {
	int n = 0;
	printf("Введите n (количество строк): ");
    scanf("%d", &n);
	if(n >= 4 && n <= 10) {
		int m = 0;
		printf("Введите m (количество столбцов): ");
		scanf("%d", &m);
		if(m >= 4 && m <= 10) {
			int X[n][m];
			for(int i = 0; i < n; i++) {
				for(int j = 0; j < m; j++) {
					printf("\nВведите элемент матрицы\n%d-я строка\n%d-й столбец\nX[%d][%d]: ", i + 1, j + 1, i + 1, j + 1);
					scanf("%d", &X[i][j]);
				} 
			}
			int result[m];
			int resultCount = 0;
			for(int j = 0; j < m; j++) {
				int min = 0;
				int max = 0;
				for(int i = 0; i < n; i++) {
					if(i == 0) {
						min = X[i][j];
						max = X[i][j];
					} else {
						if(X[i][j] < min) {
							min = X[i][j];
						}
						if(X[i][j] > max) {
							max = X[i][j];
						}
					}
				}
				int differenceMaxMin = max - min;
				if(differenceMaxMin != 0) {
					result[resultCount] = differenceMaxMin;
					resultCount += 1;
				}
			}
			printf("\nМассив ненулевых разностей максимального и минимального элементов в каждом столбце\n");
			if(resultCount == 0) {
				printf("Массив пуст, все разности равны нулю");
			} else {
				for(int i = 0; i < resultCount; i++) {
					printf("%d ", result[i]);
				} 
			}
		} else {
			printf("Количество столбцов должно быть больше или равно 4 и меньше или равно 10");
		}
	} else {
		printf("Количество строк должно быть больше или равно 4 и меньше или равно 10");
	}
	return 0;
}