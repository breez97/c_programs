#include <stdio.h>

int main() {
	int n = 0;
	printf("Введите n (количество строк и столбцов для квадратной матрицы): ");
    scanf("%d", &n);
	if(n >= 4 && n <= 10) {
		int X[n][n];
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++) {
				printf("\nВведите элемент матрицы\n%d-я строка\n%d-й столбец\nX[%d][%d]: ", i + 1, j + 1, i + 1, j + 1);
				scanf("%d", &X[i][j]);
			}
		}
		printf("\nИзначальная матрица:\n");
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++) {
				printf("%d ", X[i][j]);
				if(j == n - 1) {
					printf("\n");
				}
			}
		}
		for(int i = 0; i < n; i++) {
			int temp = X[i][i];
			X[i][i] = X[i][n - 1 - i];
			X[i][n - 1 - i] = temp;
		}
		printf("\nИтоговая матрица:\n");
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++) {
				printf("%d ", X[i][j]);
				if(j == n - 1) {
					printf("\n");
				}
			}
		}
	} else {
		printf("Количество строк и столбцов должно быть больше или равно 4 и меньше или равно 10");
	}
	return 0;
}