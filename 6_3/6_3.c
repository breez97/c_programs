#include <stdio.h>

int main() {
    int k = 0;
    scanf("%d", &k);
    int X[k];
    int Y[k];

    if(k > 0) {
        int *ptrX;
        int *ptrY;

        for(ptrX = X; ptrX < X + k; ptrX++) {
            scanf("%d", ptrX);
        }

        for(ptrY = Y; ptrY < Y + k; ptrY++) {
            scanf("%d", ptrY);
        }

        int temp = 0;

        for(ptrX = X; ptrX < X + k; ptrX++) {
            for(int *ptrX2 = ptrX + 1; ptrX2 < X + k; ptrX2++) {
                if(*ptrX < *ptrX2) {
                    temp = *ptrX;
                    *ptrX = *ptrX2;
                    *ptrX2 = temp;
                }
            }
        }

        int n = 0;
        
        for(ptrX = X; ptrX < X + k; ptrX++) {
            if(*ptrX % 2 == 0 && *ptrX < (ptrX - X)) {
                n += 1;
            }
            if((ptrX - X) % 2 != 0) {
                n += 1;
            }
        }

        int Z[n];
        int *ptrZ = Z;

        for(ptrX = X; ptrX < X + k; ptrX++) {
            if(*ptrX % 2 == 0 && *ptrX < (ptrX - X)) {
                *ptrZ = *ptrX;
                ptrZ++;
            }
        }

        for(ptrY = Y; ptrY < Y + k; ptrY++) {
            if((ptrY - Y) % 2 != 0) {
                *ptrZ = *ptrY;
                ptrZ++;
            }
        }

        printf("\nМассив Z[n]:\n");

        for(ptrZ = Z; ptrZ < Z + n; ptrZ++) {
            printf("%d ", *ptrZ);
        }
    } else {
        printf("Ошибка ввода размера массива, число должно быть больше 0\n");
    }

    return 0;
}