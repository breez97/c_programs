#include <stdio.h>
#include <stdlib.h>

int main() {
	int n = 0;
	scanf("%d", &n);
	int Y[n];

	int countMoreThanFive = 0;
	int resultMoreThanFive = 0;

	int Z[n];
	int nx = 0;

	if(n > 0) {
		int *ptrY;
		int *ptrZ;
		
		for(ptrY = Y; ptrY < Y + n; ptrY++) {
			scanf("%d", ptrY);
		}

		for (ptrY = Y; ptrY < Y + n; ptrY++) {
			if(abs(*ptrY) > 5) {
				resultMoreThanFive = *ptrY;
				countMoreThanFive += 1;
				if(countMoreThanFive == 2) {
					break;
				}
			}
		}
		
		for (ptrY = Y, ptrZ = Z + n - 1; ptrY < Y + n; ptrY++, ptrZ--) {
			*ptrZ = *ptrY;
		}

		if(countMoreThanFive < 2) {
			printf("Второго элемента, значение которого по модулю больше 5 нет.\n");
		} else {
			printf("Второй элемент, значение которого по модулю больше 5: %d\n", resultMoreThanFive);
		}

		printf("\nИнвертированный массив:\n");
		for (ptrZ = Z; ptrZ < Z + n; ptrZ++) {
			printf("%d ", *ptrZ);
		}

		for (ptrZ = Z, nx = 0; ptrZ < Z + n; ptrZ++) {
			if((ptrZ - Z) % 2 == 0) {
				Z[nx] = *ptrZ;
				nx += 1;
			}
		}

		printf("\n\nИнвертированный массив c удаленными элементами, стоящими на нечетных позициях: \n");
		for (ptrZ = Z; ptrZ < Z + nx; ptrZ++) {
			printf("%d ", *ptrZ);
		}
	} else {
		printf("Ошибка ввода размера массива, число должно быть больше 0\n");
	}

	return 0;
}