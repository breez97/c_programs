#include <stdio.h>
#include <stdlib.h>

void MakeTask(int *Y, int *Z, int n, int *resultMoreThanFive, int *countMoreThanFive, int *nx);

void FillArray(int *Y, int n);
void FindElements(int *Y, int n, int *resultMoreThanFive, int *countMoreThanFive);
void InvertArray(int *Y, int n, int *Z);
void CheckSecondElement(int countMoreThanFive, int resultMoreThanFive);
void PrintInvertArray(int *Z, int n);
void RemoveElements(int *Z, int n, int *nx);
void PrintRemoveInvertArray(int *Z, int nx);

int main() {
    int n = 0;
    scanf("%d", &n);
    int Y[n];

    int countMoreThanFive = 0;
    int resultMoreThanFive = 0;

    int Z[n];
    int nx = 0;

    MakeTask(Y, Z, n, &resultMoreThanFive, &countMoreThanFive, &nx);

    return 0;
}

void MakeTask(int *Y, int *Z, int n, int *resultMoreThanFive, int *countMoreThanFive, int *nx) {
    if (n > 0) {

        FillArray(Y, n);

        FindElements(Y, n, resultMoreThanFive, countMoreThanFive);

        InvertArray(Y, n, Z);

        CheckSecondElement(*countMoreThanFive, *resultMoreThanFive);

        PrintInvertArray(Z, n);

        RemoveElements(Z, n, nx);

        PrintRemoveInvertArray(Z, *nx);
    } else {
        printf("Ошибка ввода размера массива, число должно быть больше 0\n");
    }
}

void FillArray(int *Y, int n) {
    for (int i = 0; i < n; i++) {
        scanf("%d", &Y[i]);
    }
}

void FindElements(int *Y, int n, int *resultMoreThanFive, int *countMoreThanFive) {
    for (int i = 0; i < n; i++) {
        if (abs(Y[i]) > 5) {
            *resultMoreThanFive = Y[i];
            *countMoreThanFive += 1;
            if (*countMoreThanFive == 2) {
                break;
            }
        }
    }
}

void InvertArray(int *Y, int n, int *Z) {
    for (int i = 0; i < n; i++) {
        Z[n - i - 1] = Y[i];
    }
}

void CheckSecondElement(int countMoreThanFive, int resultMoreThanFive) {
    if (countMoreThanFive < 2) {
        printf("Второго элемента, значение которого по модулю больше 5 нет.\n");
    } else {
        printf("Второй элемент, значение которого по модулю больше 5: %d\n", resultMoreThanFive);
    }
}

void PrintInvertArray(int *Z, int n) {
    printf("\nИнвертированный массив:\n");
    for (int i = 0; i < n; i++) {
        printf("%d ", Z[i]);
    }
}

void RemoveElements(int *Z, int n, int *nx) {
    for (int i = 0; i < n; i++) {
        if (i % 2 == 0) {
            Z[*nx] = Z[i];
            *nx += 1;
        }
    }
}

void PrintRemoveInvertArray(int *Z, int nx) {
    printf("\n\nИнвертированный массив c удаленными элементами, стоящими на нечетных позициях: \n");
    for (int i = 0; i < nx; i++) {
        printf("%d ", Z[i]);
    }
}