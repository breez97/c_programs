#include <stdio.h>

void MakeTask(int *X, int *Y, int *Z, int k, int *n, int *temp, int *n_copy);

void FillArrays(int *X, int *Y, int k);
void SortXDesc(int *X, int k, int *temp);
void GetZLength(int *X, int k, int *n);
void FillZWithXElements(int *X, int *Z, int k, int *n_copy);
void FillZWithYElements(int *Y, int *Z, int k, int *n_copy);
void PrintZArray(int *Z, int n);

int main() {
    int k = 0;
    scanf("%d", &k);
    int X[k];
    int Y[k];
    int temp = 0;
    int n = 0;
    int Z[k];
    int n_copy = 0;
    
    MakeTask(X, Y, Z, k, &n, &temp, &n_copy);
    
    return 0;
}

void MakeTask(int *X, int *Y, int *Z, int k, int *n, int *temp, int *n_copy) {
    if (k > 0) {
        
        FillArrays(X, Y, k);
        
        SortXDesc(X, k, temp);
        
        GetZLength(X, k, n);
        
        FillZWithXElements(X, Z, k, n_copy);
        
        FillZWithYElements(Y, Z, k, n_copy);
        
        PrintZArray(Z, *n);
        
    } else {
        printf("Ошибка ввода размера массива, число должно быть больше 0\n");
    }
}

void FillArrays(int *X, int *Y, int k) {
    for(int i = 0; i < k; i++) {
        scanf("%d", &X[i]);
    }
    
    for(int i = 0; i < k; i++) {
        scanf("%d", &Y[i]);
    }
}

void SortXDesc(int *X, int k, int *temp) {
    for(int i = 0; i < k; i++) {
        for(int j = i + 1; j < k; j++) {
            if(X[i] < X[j]) {
                *temp = X[i];
                X[i] = X[j];
                X[j] = *temp;
            }
        }
    }
}

void GetZLength(int *X, int k, int *n) {
    *n = 0;
    for(int i = 0; i < k; i++) {
        if(X[i] % 2 == 0 && X[i] < i) {
            *n = *n + 1;
        }
        if(i % 2 != 0) {
            *n = *n + 1;
        }
    }
}

void FillZWithXElements(int *X, int *Z, int k, int *n_copy) {
    for(int i = 0; i < k; i++) {
        if(X[i] % 2 == 0 && X[i] < i) {
            Z[*n_copy] = X[i];
            *n_copy = *n_copy + 1;
        }
    }
}

void FillZWithYElements(int *Y, int *Z, int k, int *n_copy) {
    for(int i = 0; i < k; i++) {
        if(i % 2 != 0) {
            Z[*n_copy] = Y[i];
            *n_copy = *n_copy + 1;
        }
    }
}

void PrintZArray(int *Z, int n) {
    printf("\nМассив Z[n]:\n");
    for(int i = 0; i < n; i++) {
        printf("%d ", Z[i]);
    }
}
